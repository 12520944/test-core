﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testCore.Models;

namespace testCore.Helpers
{
    public class AddDefaultUserRoles
    {
        private  readonly RoleManager<IdentityRole> _roleManager;
        private  readonly UserManager<ApplicationUser> _userManager;
        public AddDefaultUserRoles(RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;

        }
        public async  void adddefaultUserRoles()
        {
            string[] listDefaultRoles = { "Admin", "Manager", "Member" };                      
            string s = "@homemarket.com";
            string[] listDefaultUsers = { "admin" + s, "manager" + s, "member" + s };
            string defaultPassword = "HomeMarket!123456";

            foreach (var role in listDefaultRoles)
            {
                
                if ((await _roleManager.RoleExistsAsync(role)) != true)
                {                    
                    await _roleManager.CreateAsync(new IdentityRole { Name = role });
                }
                foreach (var user in listDefaultUsers)
                {
                    var username = user.Split('@');
                    if ((await _userManager.FindByNameAsync(user)) == null && username[0] == role.ToLower())
                    {
                        var newUser = new ApplicationUser();
                        newUser.UserName = user;
                        newUser.Email = user;
                        var result = await _userManager.CreateAsync(newUser, defaultPassword);
                        if (result.Succeeded)
                        {
                            await _userManager.AddToRoleAsync(newUser, role);
                        }
                    }
                }
            }

        }
    }
}
