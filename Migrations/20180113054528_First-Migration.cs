﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace testCore.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "__MigrationHistory",
            //    columns: table => new
            //    {
            //        MigrationId = table.Column<string>(maxLength: 150, nullable: false),
            //        ContextKey = table.Column<string>(maxLength: 300, nullable: false),
            //        Model = table.Column<byte[]>(nullable: false),
            //        ProductVersion = table.Column<string>(maxLength: 32, nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK___MigrationHistory", x => new { x.MigrationId, x.ContextKey });
            //    });

            //migrationBuilder.CreateTable(
            //    name: "BangGiaCho",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        GiaThucPham = table.Column<double>(nullable: true),
            //        LoaiThucPham = table.Column<string>(maxLength: 50, nullable: true),
            //        MaThucPham = table.Column<string>(maxLength: 50, nullable: true),
            //        TenThucPham = table.Column<string>(maxLength: 250, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_BangGiaCho", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "BangGiaSieuThi",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        GiaThucPham = table.Column<double>(nullable: true),
            //        LoaiThucPham = table.Column<string>(maxLength: 50, nullable: true),
            //        MaThucPham = table.Column<string>(maxLength: 50, nullable: true),
            //        TenThucPham = table.Column<string>(maxLength: 250, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_BangGiaSieuThi", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "GioiThieu",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        NgayTao = table.Column<DateTime>(type: "date", nullable: true),
            //        NoiDung = table.Column<string>(type: "ntext", nullable: true),
            //        Ten = table.Column<string>(maxLength: 250, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_GioiThieu", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "KhachHang",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        DiaChi = table.Column<string>(maxLength: 1024, nullable: true),
            //        Email = table.Column<string>(maxLength: 1024, nullable: true),
            //        HinhAnh = table.Column<string>(maxLength: 1024, nullable: true),
            //        Ma = table.Column<string>(maxLength: 50, nullable: true),
            //        NgayDangKy = table.Column<DateTime>(type: "date", nullable: true),
            //        NgaySinh = table.Column<string>(maxLength: 50, nullable: true),
            //        NguoiDiCho = table.Column<bool>(nullable: true),
            //        QuocTich = table.Column<string>(maxLength: 250, nullable: true),
            //        SDT = table.Column<string>(maxLength: 50, nullable: true),
            //        Ten = table.Column<string>(maxLength: 250, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_KhachHang", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "LienHe",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        NoiDung = table.Column<string>(type: "ntext", nullable: true),
            //        TrangThai = table.Column<bool>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_LienHe", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "NguoiDiCho",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        CMND = table.Column<string>(type: "nchar(10)", nullable: true),
            //        DanhGia = table.Column<double>(nullable: true),
            //        DiaChi = table.Column<string>(maxLength: 1024, nullable: true),
            //        Email = table.Column<string>(maxLength: 1024, nullable: true),
            //        HinhAnh = table.Column<string>(maxLength: 1024, nullable: true),
            //        Ma = table.Column<string>(maxLength: 50, nullable: true),
            //        NgayDangKy = table.Column<DateTime>(type: "date", nullable: true),
            //        NgaySinh = table.Column<string>(maxLength: 50, nullable: true),
            //        QuocTich = table.Column<string>(maxLength: 250, nullable: true),
            //        SDT = table.Column<string>(maxLength: 50, nullable: true),
            //        TaiKhoan = table.Column<double>(nullable: true),
            //        Ten = table.Column<string>(maxLength: 250, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_NguoiDiCho", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "NhanDonHang",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false),
            //        MaDonHang = table.Column<string>(maxLength: 50, nullable: true),
            //        MaNguoiDiCho = table.Column<string>(maxLength: 50, nullable: true),
            //        ThoiGianNhan = table.Column<string>(maxLength: 50, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_NhanDonHang", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Role",
            //    columns: table => new
            //    {
            //        Id = table.Column<string>(maxLength: 50, nullable: false),
            //        Ten = table.Column<string>(maxLength: 50, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Role", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Students",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Address = table.Column<string>(nullable: true),
            //        BirthDay = table.Column<DateTime>(type: "datetime", nullable: false),
            //        FirstName = table.Column<string>(nullable: true),
            //        Gender = table.Column<string>(nullable: true),
            //        LastName = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Students", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "TaiKhoan",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        Ma = table.Column<string>(maxLength: 50, nullable: true),
            //        MaNguoiDiCho = table.Column<string>(maxLength: 50, nullable: true),
            //        TaiKhoan = table.Column<double>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_TaiKhoan", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "User",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        DiaChi = table.Column<string>(maxLength: 1024, nullable: true),
            //        Email = table.Column<string>(maxLength: 1024, nullable: true),
            //        GroupID = table.Column<string>(maxLength: 20, nullable: true),
            //        HinhAnh = table.Column<string>(maxLength: 1024, nullable: true),
            //        Ma = table.Column<string>(maxLength: 50, nullable: true),
            //        NgayDangKy = table.Column<DateTime>(type: "date", nullable: true),
            //        NgaySinh = table.Column<DateTime>(nullable: true),
            //        Password = table.Column<string>(maxLength: 32, nullable: true),
            //        QuocTich = table.Column<string>(maxLength: 250, nullable: true),
            //        SDT = table.Column<string>(maxLength: 50, nullable: true),
            //        Status = table.Column<bool>(nullable: false),
            //        Ten = table.Column<string>(maxLength: 250, nullable: true),
            //        UserName = table.Column<string>(maxLength: 50, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_User", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "UserGroup",
            //    columns: table => new
            //    {
            //        Id = table.Column<string>(maxLength: 20, nullable: false),
            //        Name = table.Column<string>(maxLength: 50, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_UserGroup", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "DonHang",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        DaNhan = table.Column<bool>(nullable: true),
            //        KhachHangId = table.Column<int>(nullable: true),
            //        Ma = table.Column<string>(maxLength: 50, nullable: true),
            //        NCUId = table.Column<int>(nullable: true),
            //        ThoiGianDat = table.Column<DateTime>(type: "datetime", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_DonHang", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_DonHang_KhachHang",
            //            column: x => x.KhachHangId,
            //            principalTable: "KhachHang",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "PhanHoi",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        KhachHangId = table.Column<int>(nullable: true),
            //        NgayTao = table.Column<DateTime>(type: "date", nullable: true),
            //        NoiDung = table.Column<string>(type: "ntext", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_PhanHoi", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_PhanHoi_KhachHang",
            //            column: x => x.KhachHangId,
            //            principalTable: "KhachHang",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "DonHangChiTiet",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        DonHangId = table.Column<int>(nullable: true),
            //        Gia = table.Column<double>(nullable: true),
            //        NCUId = table.Column<int>(nullable: true),
            //        SoLuong = table.Column<double>(nullable: true),
            //        TenThucPham = table.Column<string>(maxLength: 250, nullable: true),
            //        ThucPhamId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_DonHangChiTiet", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_DonHangChiTiet_DonHang",
            //            column: x => x.DonHangId,
            //            principalTable: "DonHang",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "GiaDonHang",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        DonHangId = table.Column<int>(nullable: true),
            //        PhiDichVu = table.Column<double>(nullable: true),
            //        TongTien = table.Column<double>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_GiaDonHang", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_GiaDonHang_DonHang",
            //            column: x => x.DonHangId,
            //            principalTable: "DonHang",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_DonHang_KhachHangId",
            //    table: "DonHang",
            //    column: "KhachHangId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_DonHangChiTiet_DonHangId",
            //    table: "DonHangChiTiet",
            //    column: "DonHangId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_GiaDonHang_DonHangId",
            //    table: "GiaDonHang",
            //    column: "DonHangId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_PhanHoi_KhachHangId",
            //    table: "PhanHoi",
            //    column: "KhachHangId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "__MigrationHistory");

            //migrationBuilder.DropTable(
            //    name: "BangGiaCho");

            //migrationBuilder.DropTable(
            //    name: "BangGiaSieuThi");

            //migrationBuilder.DropTable(
            //    name: "DonHangChiTiet");

            //migrationBuilder.DropTable(
            //    name: "GiaDonHang");

            //migrationBuilder.DropTable(
            //    name: "GioiThieu");

            //migrationBuilder.DropTable(
            //    name: "LienHe");

            //migrationBuilder.DropTable(
            //    name: "NguoiDiCho");

            //migrationBuilder.DropTable(
            //    name: "NhanDonHang");

            //migrationBuilder.DropTable(
            //    name: "PhanHoi");

            //migrationBuilder.DropTable(
            //    name: "Role");

            //migrationBuilder.DropTable(
            //    name: "Students");

            //migrationBuilder.DropTable(
            //    name: "TaiKhoan");

            //migrationBuilder.DropTable(
            //    name: "User");

            //migrationBuilder.DropTable(
            //    name: "UserGroup");

            //migrationBuilder.DropTable(
            //    name: "DonHang");

            //migrationBuilder.DropTable(
            //    name: "KhachHang");
        }
    }
}
