﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using testCore.Models;

namespace testCore.Controllers
{
    public class BangGiaChoController : Controller
    {
        private readonly HomeMarketContext _context;

        public BangGiaChoController(HomeMarketContext context)
        {
            _context = context;
        }

        // GET: BangGiaCho
        public async Task<IActionResult> Index()
        {
            return View(await _context.BangGiaCho.ToListAsync());
        }

        // GET: BangGiaCho/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bangGiaCho = await _context.BangGiaCho
                .SingleOrDefaultAsync(m => m.Id == id);
            if (bangGiaCho == null)
            {
                return NotFound();
            }

            return View(bangGiaCho);
        }

        // GET: BangGiaCho/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: BangGiaCho/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,MaThucPham,TenThucPham,LoaiThucPham,GiaThucPham")] BangGiaCho bangGiaCho)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bangGiaCho);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(bangGiaCho);
        }

        // GET: BangGiaCho/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bangGiaCho = await _context.BangGiaCho.SingleOrDefaultAsync(m => m.Id == id);
            if (bangGiaCho == null)
            {
                return NotFound();
            }
            return View(bangGiaCho);
        }

        // POST: BangGiaCho/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,MaThucPham,TenThucPham,LoaiThucPham,GiaThucPham")] BangGiaCho bangGiaCho)
        {
            if (id != bangGiaCho.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bangGiaCho);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BangGiaChoExists(bangGiaCho.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(bangGiaCho);
        }

        // GET: BangGiaCho/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bangGiaCho = await _context.BangGiaCho
                .SingleOrDefaultAsync(m => m.Id == id);
            if (bangGiaCho == null)
            {
                return NotFound();
            }

            return View(bangGiaCho);
        }

        // POST: BangGiaCho/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bangGiaCho = await _context.BangGiaCho.SingleOrDefaultAsync(m => m.Id == id);
            _context.BangGiaCho.Remove(bangGiaCho);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BangGiaChoExists(int id)
        {
            return _context.BangGiaCho.Any(e => e.Id == id);
        }
    }
}
