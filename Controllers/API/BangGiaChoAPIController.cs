﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using testCore.Models;

namespace testCore.Controllers.API
{
    [Produces("application/json")]
    [Route("api/BangGiaChoAPI")]
    public class BangGiaChoAPIController : Controller
    {
        private readonly HomeMarketContext _context;

        public BangGiaChoAPIController(HomeMarketContext context)
        {
            _context = context;
        }

        // GET: api/BangGiaChoAPI
        [HttpGet]
        public IEnumerable<BangGiaCho> GetBangGiaCho()
        {
            return _context.BangGiaCho;
        }

        // GET: api/BangGiaChoAPI/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBangGiaCho([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bangGiaCho = await _context.BangGiaCho.SingleOrDefaultAsync(m => m.Id == id);

            if (bangGiaCho == null)
            {
                return NotFound();
            }

            return Ok(bangGiaCho);
        }

        // PUT: api/BangGiaChoAPI/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBangGiaCho([FromRoute] int id, [FromBody] BangGiaCho bangGiaCho)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bangGiaCho.Id)
            {
                return BadRequest();
            }

            _context.Entry(bangGiaCho).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BangGiaChoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BangGiaChoAPI
        [HttpPost]
        public async Task<IActionResult> PostBangGiaCho([FromBody] BangGiaCho bangGiaCho)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.BangGiaCho.Add(bangGiaCho);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBangGiaCho", new { id = bangGiaCho.Id }, bangGiaCho);
        }

        // DELETE: api/BangGiaChoAPI/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBangGiaCho([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bangGiaCho = await _context.BangGiaCho.SingleOrDefaultAsync(m => m.Id == id);
            if (bangGiaCho == null)
            {
                return NotFound();
            }

            _context.BangGiaCho.Remove(bangGiaCho);
            await _context.SaveChangesAsync();

            return Ok(bangGiaCho);
        }

        private bool BangGiaChoExists(int id)
        {
            return _context.BangGiaCho.Any(e => e.Id == id);
        }
    }
}