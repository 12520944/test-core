﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class LienHe
    {
        public int Id { get; set; }
        public string NoiDung { get; set; }
        public bool? TrangThai { get; set; }
    }
}
