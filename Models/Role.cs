﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class Role
    {
        public string Id { get; set; }
        public string Ten { get; set; }
    }
}
