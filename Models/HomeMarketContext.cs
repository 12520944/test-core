﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace testCore.Models
{
    public partial class HomeMarketContext : IdentityDbContext<ApplicationUser>
    {       
        public virtual DbSet<BangGiaCho> BangGiaCho { get; set; }
        public virtual DbSet<BangGiaSieuThi> BangGiaSieuThi { get; set; }
        public virtual DbSet<DonHang> DonHang { get; set; }
        public virtual DbSet<DonHangChiTiet> DonHangChiTiet { get; set; }
        public virtual DbSet<GiaDonHang> GiaDonHang { get; set; }
        public virtual DbSet<GioiThieu> GioiThieu { get; set; }
        public virtual DbSet<KhachHang> KhachHang { get; set; }
        public virtual DbSet<LienHe> LienHe { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<NguoiDiCho> NguoiDiCho { get; set; }
        public virtual DbSet<NhanDonHang> NhanDonHang { get; set; }
        public virtual DbSet<PhanHoi> PhanHoi { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Students> Students { get; set; }
        public virtual DbSet<TaiKhoan> TaiKhoan { get; set; }
        public virtual DbSet<UserGroup> UserGroup { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        public HomeMarketContext(DbContextOptions<HomeMarketContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

           
            modelBuilder.Entity<BangGiaCho>(entity =>
            {
                entity.Property(e => e.LoaiThucPham).HasMaxLength(50);

                entity.Property(e => e.MaThucPham).HasMaxLength(50);

                entity.Property(e => e.TenThucPham).HasMaxLength(250);
            });

            modelBuilder.Entity<BangGiaSieuThi>(entity =>
            {
                entity.Property(e => e.LoaiThucPham).HasMaxLength(50);

                entity.Property(e => e.MaThucPham).HasMaxLength(50);

                entity.Property(e => e.TenThucPham).HasMaxLength(250);
            });

            modelBuilder.Entity<DonHang>(entity =>
            {
                entity.Property(e => e.Ma).HasMaxLength(50);

                entity.Property(e => e.Ncuid).HasColumnName("NCUId");

                entity.Property(e => e.ThoiGianDat).HasColumnType("datetime");

                entity.HasOne(d => d.KhachHang)
                    .WithMany(p => p.DonHang)
                    .HasForeignKey(d => d.KhachHangId)
                    .HasConstraintName("FK_DonHang_KhachHang");
            });

            modelBuilder.Entity<DonHangChiTiet>(entity =>
            {
                entity.Property(e => e.Ncuid).HasColumnName("NCUId");

                entity.Property(e => e.TenThucPham).HasMaxLength(250);

                entity.HasOne(d => d.DonHang)
                    .WithMany(p => p.DonHangChiTiet)
                    .HasForeignKey(d => d.DonHangId)
                    .HasConstraintName("FK_DonHangChiTiet_DonHang");
            });

            modelBuilder.Entity<GiaDonHang>(entity =>
            {
                entity.HasOne(d => d.DonHang)
                    .WithMany(p => p.GiaDonHang)
                    .HasForeignKey(d => d.DonHangId)
                    .HasConstraintName("FK_GiaDonHang_DonHang");
            });

            modelBuilder.Entity<GioiThieu>(entity =>
            {
                entity.Property(e => e.NgayTao).HasColumnType("date");

                entity.Property(e => e.NoiDung).HasColumnType("ntext");

                entity.Property(e => e.Ten).HasMaxLength(250);
            });

            modelBuilder.Entity<KhachHang>(entity =>
            {
                entity.Property(e => e.DiaChi).HasMaxLength(1024);

                entity.Property(e => e.Email).HasMaxLength(1024);

                entity.Property(e => e.HinhAnh).HasMaxLength(1024);

                entity.Property(e => e.Ma).HasMaxLength(50);

                entity.Property(e => e.NgayDangKy).HasColumnType("date");

                entity.Property(e => e.NgaySinh).HasMaxLength(50);

                entity.Property(e => e.QuocTich).HasMaxLength(250);

                entity.Property(e => e.Sdt)
                    .HasColumnName("SDT")
                    .HasMaxLength(50);

                entity.Property(e => e.Ten).HasMaxLength(250);
            });

            modelBuilder.Entity<LienHe>(entity =>
            {
                entity.Property(e => e.NoiDung).HasColumnType("ntext");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey });

                entity.ToTable("__MigrationHistory");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<NguoiDiCho>(entity =>
            {
                entity.Property(e => e.Cmnd)
                    .HasColumnName("CMND")
                    .HasColumnType("nchar(10)");

                entity.Property(e => e.DiaChi).HasMaxLength(1024);

                entity.Property(e => e.Email).HasMaxLength(1024);

                entity.Property(e => e.HinhAnh).HasMaxLength(1024);

                entity.Property(e => e.Ma).HasMaxLength(50);

                entity.Property(e => e.NgayDangKy).HasColumnType("date");

                entity.Property(e => e.NgaySinh).HasMaxLength(50);

                entity.Property(e => e.QuocTich).HasMaxLength(250);

                entity.Property(e => e.Sdt)
                    .HasColumnName("SDT")
                    .HasMaxLength(50);

                entity.Property(e => e.Ten).HasMaxLength(250);
            });

            modelBuilder.Entity<NhanDonHang>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.MaDonHang).HasMaxLength(50);

                entity.Property(e => e.MaNguoiDiCho).HasMaxLength(50);

                entity.Property(e => e.ThoiGianNhan).HasMaxLength(50);
            });

            modelBuilder.Entity<PhanHoi>(entity =>
            {
                entity.Property(e => e.NgayTao).HasColumnType("date");

                entity.Property(e => e.NoiDung).HasColumnType("ntext");

                entity.HasOne(d => d.KhachHang)
                    .WithMany(p => p.PhanHoi)
                    .HasForeignKey(d => d.KhachHangId)
                    .HasConstraintName("FK_PhanHoi_KhachHang");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.Ten).HasMaxLength(50);
            });

            modelBuilder.Entity<Students>(entity =>
            {
                entity.Property(e => e.BirthDay).HasColumnType("datetime");
            });

            modelBuilder.Entity<TaiKhoan>(entity =>
            {
                entity.Property(e => e.Ma).HasMaxLength(50);

                entity.Property(e => e.MaNguoiDiCho).HasMaxLength(50);

                entity.Property(e => e.TaiKhoan1).HasColumnName("TaiKhoan");
            });

            modelBuilder.Entity<UserGroup>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(20)
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.DiaChi).HasMaxLength(1024);

                entity.Property(e => e.Email).HasMaxLength(1024);

                entity.Property(e => e.GroupId)
                    .HasColumnName("GroupID")
                    .HasMaxLength(20);

                entity.Property(e => e.HinhAnh).HasMaxLength(1024);

                entity.Property(e => e.Ma).HasMaxLength(50);

                entity.Property(e => e.NgayDangKy).HasColumnType("date");

                entity.Property(e => e.Password).HasMaxLength(32);

                entity.Property(e => e.QuocTich).HasMaxLength(250);

                entity.Property(e => e.Sdt)
                    .HasColumnName("SDT")
                    .HasMaxLength(50);

                entity.Property(e => e.Ten).HasMaxLength(250);

                entity.Property(e => e.UserName).HasMaxLength(50);
            });
        }
    }
}
