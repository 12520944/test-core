﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class NguoiDiCho
    {
        public int Id { get; set; }
        public string Ma { get; set; }
        public string Ten { get; set; }
        public string HinhAnh { get; set; }
        public string NgaySinh { get; set; }
        public string QuocTich { get; set; }
        public string Sdt { get; set; }
        public string Cmnd { get; set; }
        public string DiaChi { get; set; }
        public string Email { get; set; }
        public double? DanhGia { get; set; }
        public double? TaiKhoan { get; set; }
        public DateTime? NgayDangKy { get; set; }
    }
}
