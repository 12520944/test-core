﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class KhachHang
    {
        public KhachHang()
        {
            DonHang = new HashSet<DonHang>();
            PhanHoi = new HashSet<PhanHoi>();
        }

        public int Id { get; set; }
        public string Ma { get; set; }
        public string Ten { get; set; }
        public string HinhAnh { get; set; }
        public string NgaySinh { get; set; }
        public string QuocTich { get; set; }
        public string Sdt { get; set; }
        public string DiaChi { get; set; }
        public string Email { get; set; }
        public DateTime? NgayDangKy { get; set; }
        public bool? NguoiDiCho { get; set; }

        public ICollection<DonHang> DonHang { get; set; }
        public ICollection<PhanHoi> PhanHoi { get; set; }
    }
}
