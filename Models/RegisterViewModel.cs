﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace testCore.Models
{
    public class RegisterViewModel
    {            
        [Required, EmailAddress , MaxLength(256),DisplayName("Email Address")]
        public string Email { get; set; }
        [Required,MinLength(6),MaxLength(50),DataType(DataType.Password),DisplayName("Password")]
        public string Password { get; set; }
        [Required, MinLength(6), MaxLength(50), DataType(DataType.Password), DisplayName("Confirm Password")]
        [Compare("Password",ErrorMessage = "The password does not match the confirmation password.")]
        public string ConfirmPassword { get; set; }
    }
}
