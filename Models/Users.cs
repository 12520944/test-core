﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class Users
    {
        public int Id { get; set; }
        public string Ma { get; set; }
        public string Ten { get; set; }
        public string HinhAnh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public string QuocTich { get; set; }
        public string Sdt { get; set; }
        public string DiaChi { get; set; }
        public string Email { get; set; }
        public DateTime? NgayDangKy { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string GroupId { get; set; }
        public bool Status { get; set; }
    }
}
