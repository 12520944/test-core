﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class PhanHoi
    {
        public int Id { get; set; }
        public int? KhachHangId { get; set; }
        public string NoiDung { get; set; }
        public DateTime? NgayTao { get; set; }

        public KhachHang KhachHang { get; set; }
    }
}
