﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class BangGiaSieuThi
    {
        public int Id { get; set; }
        public string MaThucPham { get; set; }
        public string TenThucPham { get; set; }
        public string LoaiThucPham { get; set; }
        public double? GiaThucPham { get; set; }
    }
}
