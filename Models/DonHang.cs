﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class DonHang
    {
        public DonHang()
        {
            DonHangChiTiet = new HashSet<DonHangChiTiet>();
            GiaDonHang = new HashSet<GiaDonHang>();
        }

        public int Id { get; set; }
        public string Ma { get; set; }
        public int? KhachHangId { get; set; }
        public int? Ncuid { get; set; }
        public DateTime? ThoiGianDat { get; set; }
        public bool? DaNhan { get; set; }

        public KhachHang KhachHang { get; set; }
        public ICollection<DonHangChiTiet> DonHangChiTiet { get; set; }
        public ICollection<GiaDonHang> GiaDonHang { get; set; }
    }
}
