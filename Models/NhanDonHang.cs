﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class NhanDonHang
    {
        public int Id { get; set; }
        public string MaDonHang { get; set; }
        public string MaNguoiDiCho { get; set; }
        public string ThoiGianNhan { get; set; }
    }
}
