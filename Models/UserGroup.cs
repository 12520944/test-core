﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class UserGroup
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
