﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class GioiThieu
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public string NoiDung { get; set; }
        public DateTime? NgayTao { get; set; }
    }
}
