﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class GiaDonHang
    {
        public int Id { get; set; }
        public int? DonHangId { get; set; }
        public double? TongTien { get; set; }
        public double? PhiDichVu { get; set; }

        public DonHang DonHang { get; set; }
    }
}
