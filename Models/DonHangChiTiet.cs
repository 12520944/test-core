﻿using System;
using System.Collections.Generic;

namespace testCore.Models
{
    public partial class DonHangChiTiet
    {
        public int Id { get; set; }
        public int? DonHangId { get; set; }
        public int? ThucPhamId { get; set; }
        public string TenThucPham { get; set; }
        public double? SoLuong { get; set; }
        public double? Gia { get; set; }
        public int? Ncuid { get; set; }

        public DonHang DonHang { get; set; }
    }
}
